# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2022-01-07)


### Features

* added localstack and start script ([9a4d443](https://bitbucket.org/mobly/laas-gateway/commit/9a4d443190598722f9b2d3b7746d950903aaa053))


### Bug Fixes

* npm install dependencies ([43e8c09](https://bitbucket.org/mobly/laas-gateway/commit/43e8c09b1f2bfd22c6ebbfdd46925c1794a6ba22))
* **serverless.ts:** arrumado variaveis pra ambiente prod ([aecc072](https://bitbucket.org/mobly/laas-gateway/commit/aecc072e40b949af263c9473d45c764659f5b93e))

## 1.2.0 (2021-10-20)


### Features

* acrescentando comentario esclarecendo propriedades do JSON recebido ([13a1ab0](https://bitbucket.org/mobly/laas-gateway/commit/13a1ab077bd47a1d1cef89e850d0a8b862f024f9))
* added barcode logic ([c8d03b7](https://bitbucket.org/mobly/laas-gateway/commit/c8d03b7b2268f4a4e45a813f174b4972bb91c33a))
* added localstack and start script ([9a4d443](https://bitbucket.org/mobly/laas-gateway/commit/9a4d443190598722f9b2d3b7746d950903aaa053))
* added policies middleware ([6b76c71](https://bitbucket.org/mobly/laas-gateway/commit/6b76c71c5f31201d4efbdf2fe52a635080fbfa71))
* added properties for Delivery and changing how the project starts ([89ca2a1](https://bitbucket.org/mobly/laas-gateway/commit/89ca2a1c905eca1d8995cae2e3258981228934ef))
* added quantity property ([420b8b8](https://bitbucket.org/mobly/laas-gateway/commit/420b8b8e393de98bd521b4a1eaa24dc66891d9c6))
* added todo reminder ([fbce690](https://bitbucket.org/mobly/laas-gateway/commit/fbce6903538b76d9b96eb7f7ed3b33d9d25a9a0e))
* adding again width and height values from volumes ([1d46cab](https://bitbucket.org/mobly/laas-gateway/commit/1d46caba1eaa95280bb08285a0ccac2f6e39306f))
* criando schema de validaçao do EDI no formato JSON ([7d6aac5](https://bitbucket.org/mobly/laas-gateway/commit/7d6aac5cdcf72b3a49bd67a7572d622b3ce0204d))
* criando schema de validaçao para o json do EDI ([f57bf1f](https://bitbucket.org/mobly/laas-gateway/commit/f57bf1fb4f9af871bfe85a410fdcc86a08adbaf4))
* dividing product weight and cubed weight by products length ([a6b85ee](https://bitbucket.org/mobly/laas-gateway/commit/a6b85ee093f40417a539eb845183b9c8d321aeb4))
* implementando lambda function para trigger s3 ([69ad662](https://bitbucket.org/mobly/laas-gateway/commit/69ad662713df3427f896bfe6dc0fbe6589a1e981))
* implementando validaçao de xml e json ([d1a81e4](https://bitbucket.org/mobly/laas-gateway/commit/d1a81e40e5f4e60f2819afa3af428246b58b074e))
* implementando versao com funcionalidade completa do fluxo inicial laas ([2187c82](https://bitbucket.org/mobly/laas-gateway/commit/2187c82fbf2fbcee517c62096a3289572ec85974))
* removing "Nfe" from xml id ([c1b7535](https://bitbucket.org/mobly/laas-gateway/commit/c1b7535ab7156dab34ba878b7ceca8e81cc425ff))
* removing json parser middleware ([5bf11ec](https://bitbucket.org/mobly/laas-gateway/commit/5bf11ecc7d97fe9c37c1db0bf934e38a6e2a142e))
* start json validation ([4d40405](https://bitbucket.org/mobly/laas-gateway/commit/4d40405cf44afccbffd7f4ada468ff256aae99d0))
* transforming xml and json ([517688c](https://bitbucket.org/mobly/laas-gateway/commit/517688c21b57d62fecb5e021655f5c2061e6cf1f))


### Bug Fixes

* acrescentando validaçao de arquivo XML e JSON no EDI recebido ([9594415](https://bitbucket.org/mobly/laas-gateway/commit/9594415315bf8086a844bb1893889e2a75cd8b61))
* corrigindo arquivo serveless e acrescentando script de deploy local no package.json ([693ceda](https://bitbucket.org/mobly/laas-gateway/commit/693ceda74d80781c150a6277bdb08adf7a4dde8e))
* corrigindo arquivos para deploy ([096189f](https://bitbucket.org/mobly/laas-gateway/commit/096189f63dc51ec55a037ffd02e6d21fcf849df4))
* corrigindo schema de validaçao JSON edi ([52be02b](https://bitbucket.org/mobly/laas-gateway/commit/52be02b3af5e7a35c12bf24b4689d3beda948ae2))
* corrigindo schema de validaçao JSON edi ([9665164](https://bitbucket.org/mobly/laas-gateway/commit/966516413002880e8f51996c716b0d1866bf6578))
* corrigindo schema de validaçao JSON edi ([8a9bdfb](https://bitbucket.org/mobly/laas-gateway/commit/8a9bdfb95d1d2afae526ca337243f27bb9921bcb))
* corrigindo serverless.ts e nome do handler s3 ([777e947](https://bitbucket.org/mobly/laas-gateway/commit/777e947961ac5b2cd58a8febe262feb3e4ab4a49))
* corrigindo variaveis de ambiente do lambda s3 e configuraçoes do arquivo serveless ([f451fa5](https://bitbucket.org/mobly/laas-gateway/commit/f451fa5a19a6ae12a3983cc6b60964434f856f93))
* fixed date to string typing ([d594599](https://bitbucket.org/mobly/laas-gateway/commit/d5945995a8c7f30b040c6de8cdb433f0eecd2410))
* fixed stuff when testing with a real xml ([97362de](https://bitbucket.org/mobly/laas-gateway/commit/97362dec38c832abdd40fb86fdb1c2815e6ffd0c))
* fixed typing for format return message ([964cc0c](https://bitbucket.org/mobly/laas-gateway/commit/964cc0ccf1d04dedf09d17a06ef194c214e41aa5))
* npm install dependencies ([43e8c09](https://bitbucket.org/mobly/laas-gateway/commit/43e8c09b1f2bfd22c6ebbfdd46925c1794a6ba22))
* removing quantity ([2e3b767](https://bitbucket.org/mobly/laas-gateway/commit/2e3b7675e2a58c61ed3da78f76d65c54e92ab6a6))

## 1.1.0 (2021-10-20)

### Features

* acrescentando comentario esclarecendo propriedades do JSON recebido ([13a1ab0](https://bitbucket.org/mobly/laas-gateway/commit/13a1ab077bd47a1d1cef89e850d0a8b862f024f9))
* added barcode logic ([c8d03b7](https://bitbucket.org/mobly/laas-gateway/commit/c8d03b7b2268f4a4e45a813f174b4972bb91c33a))
* added localstack and start script ([9a4d443](https://bitbucket.org/mobly/laas-gateway/commit/9a4d443190598722f9b2d3b7746d950903aaa053))
* added policies middleware ([6b76c71](https://bitbucket.org/mobly/laas-gateway/commit/6b76c71c5f31201d4efbdf2fe52a635080fbfa71))
* added properties for Delivery and changing how the project starts ([89ca2a1](https://bitbucket.org/mobly/laas-gateway/commit/89ca2a1c905eca1d8995cae2e3258981228934ef))
* added quantity property ([420b8b8](https://bitbucket.org/mobly/laas-gateway/commit/420b8b8e393de98bd521b4a1eaa24dc66891d9c6))
* added todo reminder ([fbce690](https://bitbucket.org/mobly/laas-gateway/commit/fbce6903538b76d9b96eb7f7ed3b33d9d25a9a0e))
* adding again width and height values from volumes ([1d46cab](https://bitbucket.org/mobly/laas-gateway/commit/1d46caba1eaa95280bb08285a0ccac2f6e39306f))
* criando schema de validaçao do EDI no formato JSON ([7d6aac5](https://bitbucket.org/mobly/laas-gateway/commit/7d6aac5cdcf72b3a49bd67a7572d622b3ce0204d))
* criando schema de validaçao para o json do EDI ([f57bf1f](https://bitbucket.org/mobly/laas-gateway/commit/f57bf1fb4f9af871bfe85a410fdcc86a08adbaf4))
* dividing product weight and cubed weight by products length ([a6b85ee](https://bitbucket.org/mobly/laas-gateway/commit/a6b85ee093f40417a539eb845183b9c8d321aeb4))
* implementando lambda function para trigger s3 ([69ad662](https://bitbucket.org/mobly/laas-gateway/commit/69ad662713df3427f896bfe6dc0fbe6589a1e981))
* implementando validaçao de xml e json ([d1a81e4](https://bitbucket.org/mobly/laas-gateway/commit/d1a81e40e5f4e60f2819afa3af428246b58b074e))
* implementando versao com funcionalidade completa do fluxo inicial laas ([2187c82](https://bitbucket.org/mobly/laas-gateway/commit/2187c82fbf2fbcee517c62096a3289572ec85974))
* removing "Nfe" from xml id ([c1b7535](https://bitbucket.org/mobly/laas-gateway/commit/c1b7535ab7156dab34ba878b7ceca8e81cc425ff))
* removing json parser middleware ([5bf11ec](https://bitbucket.org/mobly/laas-gateway/commit/5bf11ecc7d97fe9c37c1db0bf934e38a6e2a142e))
* start json validation ([4d40405](https://bitbucket.org/mobly/laas-gateway/commit/4d40405cf44afccbffd7f4ada468ff256aae99d0))
* transforming xml and json ([517688c](https://bitbucket.org/mobly/laas-gateway/commit/517688c21b57d62fecb5e021655f5c2061e6cf1f))


### Bug Fixes

* acrescentando validaçao de arquivo XML e JSON no EDI recebido ([9594415](https://bitbucket.org/mobly/laas-gateway/commit/9594415315bf8086a844bb1893889e2a75cd8b61))
* corrigindo arquivo serveless e acrescentando script de deploy local no package.json ([693ceda](https://bitbucket.org/mobly/laas-gateway/commit/693ceda74d80781c150a6277bdb08adf7a4dde8e))
* corrigindo arquivos para deploy ([096189f](https://bitbucket.org/mobly/laas-gateway/commit/096189f63dc51ec55a037ffd02e6d21fcf849df4))
* corrigindo schema de validaçao JSON edi ([52be02b](https://bitbucket.org/mobly/laas-gateway/commit/52be02b3af5e7a35c12bf24b4689d3beda948ae2))
* corrigindo schema de validaçao JSON edi ([9665164](https://bitbucket.org/mobly/laas-gateway/commit/966516413002880e8f51996c716b0d1866bf6578))
* corrigindo schema de validaçao JSON edi ([8a9bdfb](https://bitbucket.org/mobly/laas-gateway/commit/8a9bdfb95d1d2afae526ca337243f27bb9921bcb))
* corrigindo serverless.ts e nome do handler s3 ([777e947](https://bitbucket.org/mobly/laas-gateway/commit/777e947961ac5b2cd58a8febe262feb3e4ab4a49))
* corrigindo variaveis de ambiente do lambda s3 e configuraçoes do arquivo serveless ([f451fa5](https://bitbucket.org/mobly/laas-gateway/commit/f451fa5a19a6ae12a3983cc6b60964434f856f93))
* fixed date to string typing ([d594599](https://bitbucket.org/mobly/laas-gateway/commit/d5945995a8c7f30b040c6de8cdb433f0eecd2410))
* fixed stuff when testing with a real xml ([97362de](https://bitbucket.org/mobly/laas-gateway/commit/97362dec38c832abdd40fb86fdb1c2815e6ffd0c))
* fixed typing for format return message ([964cc0c](https://bitbucket.org/mobly/laas-gateway/commit/964cc0ccf1d04dedf09d17a06ef194c214e41aa5))
* npm install dependencies ([43e8c09](https://bitbucket.org/mobly/laas-gateway/commit/43e8c09b1f2bfd22c6ebbfdd46925c1794a6ba22))
* removing quantity ([2e3b767](https://bitbucket.org/mobly/laas-gateway/commit/2e3b7675e2a58c61ed3da78f76d65c54e92ab6a6))
