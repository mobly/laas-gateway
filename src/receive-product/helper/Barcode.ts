import { InfNFe } from '@functions/create-order/dto/nfe.dto';

export class Barcode {
  private readonly companyCnpj: string;
  private readonly invoiceNumber: string;
  private readonly invoiceSerieNumber: string;
  constructor(infNFe: InfNFe) {
    this.companyCnpj = infNFe.emit.CNPJ;
    this.invoiceNumber = this.completeNumberWithZeros(infNFe.ide.nNF, {
      length: 9,
    });
    this.invoiceSerieNumber = this.completeNumberWithZeros(infNFe.ide.serie, {
      length: 2,
    });
  }

  static of(infNFe: InfNFe) {
    return new Barcode(infNFe);
  }

  generateBarCode(itemNumber: string): string {
    const itemNumberWithZeros = this.completeNumberWithZeros(itemNumber, {
      length: 3,
    });
    return `${this.companyCnpj}${this.invoiceNumber}${this.invoiceSerieNumber}${itemNumberWithZeros}`;
  }

  private completeNumberWithZeros(
    number: string,
    { length }: { length: number },
  ): string {
    return Array.from<undefined>({
      length: number.length - length,
    }).reduce((acc: string) => `0${acc}`, number);
  }
}
