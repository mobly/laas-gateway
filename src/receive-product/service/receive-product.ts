import { v4 as uuidv4 } from 'uuid';
import { Det, InfNFe, Nfe } from '@functions/create-order/dto/nfe.dto';
import {
  Delivery,
  Product,
  Volume,
} from '@functions/create-order/dto/nfeHub.dto';
import { getQueueProducerByQueueName } from '@libs/getQueueProducer';
import { Barcode } from '@receiveProduct/helper/Barcode';

const receiveLaaSDataQueueName = process.env.RECEIVE_LAAS_DATA_QUEUE_NAME;

export async function receiveProduct(nfeJson: Nfe) {
  const delivery = nfeJsonToHubJson(nfeJson);
  const producer = await getQueueProducerByQueueName(receiveLaaSDataQueueName);
  await producer.send({
    id: uuidv4(),
    body: JSON.stringify(delivery),
  });
}

function nfeJsonToHubJson(nfeJson: Nfe): Delivery {
  return {
    package: getOnlyXmlIdNumbers(nfeJson.nfeProc.NFe.infNFe.Id),
    freightValue: nfeJson.nfeProc.NFe.infNFe.total.ICMSTot.vFrete,
    typeShipping: 's', // S (direto pro cliente) ou P (clica e retira) ou R (reversa)
    carrierDocument: nfeJson.nfeProc.NFe.infNFe.transp.transporta.CNPJ,
    carrierCode: 'MOB', //MOB,
    partner: {
      cnpj: nfeJson.nfeProc.NFe.infNFe.emit.CNPJ,
      companyName: nfeJson.nfeProc.NFe.infNFe.emit.xNome,
      tradingName: nfeJson.nfeProc.NFe.infNFe.emit.xFant,
    },
    customer: {
      name: nfeJson.nfeProc.NFe.infNFe.dest.xNome,
      email: '', //campo opcional da nota
      phone: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.fone,
      document: nfeJson.nfeProc.NFe.infNFe.dest.CPF,
      address: {
        street: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.xLgr,
        number: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.nro,
        postcode: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.CEP,
        neighborhood: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.xBairro,
        city: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.xMun,
        state: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.UF,
        complement: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.xCpl,
        country: nfeJson.nfeProc.NFe.infNFe.dest.enderDest.xPais,
        reference: '', // opcional da nota
        type: '', // campo opcional na nota
      },
      invoice: {
        key: getOnlyXmlIdNumbers(nfeJson.nfeProc.NFe.infNFe.Id),
        originState: nfeJson.nfeProc.NFe.infNFe.emit.enderEmit.UF,
        originZipCode: nfeJson.nfeProc.NFe.infNFe.emit.enderEmit.CEP,
        serie: nfeJson.nfeProc.NFe.infNFe.ide.serie,
        number: nfeJson.nfeProc.NFe.infNFe.ide.cNF,
        total: nfeJson.nfeProc.NFe.infNFe.total.ICMSTot.vBC,
        date: nfeJson.nfeProc.NFe.infNFe.ide.dhEmi,
        identifier: getOnlyXmlIdNumbers(nfeJson.nfeProc.NFe.infNFe.Id), //número do pedido do cliente (avaliar)
        products: generateProductsFromNfe(nfeJson.nfeProc.NFe.infNFe.det),
      },
      volumes: generateVolumesFromNfe(nfeJson.nfeProc.NFe.infNFe),
    },
  };
}

function generateProductsFromNfe(NfeDetProperty: Det | Det[]): Product[] {
  let products = !Array.isArray(NfeDetProperty)
    ? [NfeDetProperty]
    : NfeDetProperty;
  return products.map<Product>((product) => ({
    code: product.prod.cProd,
    name: product.prod.xProd,
  }));
}

function generateVolumesFromNfe(infNFe: InfNFe): Volume[] {
  let products = !Array.isArray(infNFe.det) ? [infNFe.det] : infNFe.det;
  return products.map<Volume>((product: Det) => ({
    productCode: product.prod.cProd,
    barcode:
      product.barcode ?? Barcode.of(infNFe).generateBarCode(product.nItem),
    cubedWeight: addPrecision(
      Number(infNFe.transp.vol.pesoB) / products.length,
    ),
    weight: addPrecision(Number(infNFe.transp.vol.pesoL) / products.length),
    length: infNFe.transp.vol.nVol,
    height: '0.0',
    width: '0.0',
  }));
}

function addPrecision(number: number): string {
  const numberAsString = String(number);
  const hasPrecision = numberAsString.indexOf('.');
  if (hasPrecision !== -1) return String(number);
  return number.toPrecision(numberAsString.length + 1);
}

function getOnlyXmlIdNumbers(nfeId: string): string {
  return nfeId.substr(3, nfeId.length);
}
