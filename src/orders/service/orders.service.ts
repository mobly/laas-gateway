import { getAll, getByID, getTrackings } from '@libs/laas-tms/client';
import {
  GetAllParams,
  GetAllResponse,
  GetByIdParams,
  GetAllOrder,
  GetTrackingsByIdParams,
  GetTrackingsByIdResponse,
} from '../../libs/laas-tms/order';

import { Metadata } from '@grpc/grpc-js';
import { TrackingNotFoundError } from '@libs/error/notFound.error';
import { InternalServerError } from '@libs/error/internalServerError.error';
const CODE_NOT_FOUND = 5;

export interface GetOrdersParams {
  partnerCnpj?: string;
  from: string;
  to: string;
  ordersNumbers: string[];
  invoiceNumbers: string[];
  page?: number;
  limit?: number;
}

export interface GetAllOrdersParams {
  from: string;
  to: string;
  ordersNumbers: string[];
  invoiceNumbers: string[];
  page?: number;
  limit?: number;
}

export interface GetTrackingParams {
  id: string;
  partnerCnpj: string;
}

export interface GetOrderParamsOptions {
  authorization: string;
}

export async function getOrders(
  {
    partnerCnpj,
    to,
    from,
    ordersNumbers,
    invoiceNumbers,
    page,
    limit,
  }: GetOrdersParams,
  { authorization }: GetOrderParamsOptions,
): Promise<GetAllResponse> {
  const getAllParams: GetAllParams = {
    to,
    from,
    partnerCnpj,
    ordersNumbers: ordersNumbers ?? [],
    invoiceNumbers: invoiceNumbers ?? [],
    page: page ?? 1,
    limit: limit ?? 10,
  };

  const metadata = new Metadata();
  metadata.set('Authorization', authorization);

  console.log('fazendo a requisição pra pegar as orders');

  const result = await getAll(getAllParams, metadata);
  console.log('retornou');

  return result;
}

export async function getOrderById(
  id: number,
  { authorization }: GetOrderParamsOptions,
): Promise<GetAllOrder> {
  const params: GetByIdParams = { id };
  const metadata = new Metadata();
  metadata.set('Authorization', authorization);

  const result = await getByID(params, metadata);
  return result;
}

export async function getTracking(
  { id, partnerCnpj }: GetTrackingParams,
  { authorization }: GetOrderParamsOptions,
): Promise<GetTrackingsByIdResponse> {
  try {
    const params: GetTrackingsByIdParams = { id, partnerCnpj };
    const metadata = new Metadata();
    metadata.set('Authorization', authorization);

    const result = await getTrackings(params, metadata);
    return result;
  } catch (error) {
    if (error.code === CODE_NOT_FOUND) {
      throw new TrackingNotFoundError();
    }
    console.log(JSON.stringify(error))
    throw new InternalServerError();
  }
}
