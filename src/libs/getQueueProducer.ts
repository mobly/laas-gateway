import * as AWS from 'aws-sdk';
import { Producer } from 'sqs-producer';

const SQS = new AWS.SQS({
  apiVersion: '2012-11-05',
  region: 'us-east-1',
  endpoint: process.env.SQS_ENDPOINT,
});

const producers: Map<string, Producer> = new Map<string, Producer>();

export async function getQueueProducerByQueueName(
  queueName: string,
): Promise<Producer> {
  if (producers.has(queueName)) return producers.get(queueName);
  const { QueueUrl: queueUrl } = await SQS.getQueueUrl({
    QueueName: queueName,
  }).promise();
  const producer = Producer.create({
    queueUrl,
    region: 'us-east-1',
  });
  producers.set(queueName, producer);
  return producer;
}
