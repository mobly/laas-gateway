import axios, { AxiosError } from 'axios';
import { UserJWTDTO } from '@libs/dto/userJwt.dto';
import { formatResponse } from '@libs/apiGateway';

export function policiesMiddleware(...policies: string[]) {
  const policiesMiddlewareBefore = async ({ event }) => {
    const authorizationHeader = event.headers['Authorization'];
    const securityUrl = process.env.SECURITY_OAUTH_TOKEN_DETAILS_URL;
    let user: UserJWTDTO;
    try {
      const { data } = await axios.post(securityUrl, null, {
        headers: { authorization: authorizationHeader },
      });
      user = data;
    } catch (e) {
      // TODO checar como esses erros são retornados
      if (axios.isAxiosError(e)) {
        const error: AxiosError = e;
        if (error.response.status === 401) return formatResponse({
          statusCode: 401,
          response: 'Unauthorized',
          contentType: 'Text/plain',
        });
      }
      formatResponse({
        statusCode: 500,
        response: 'Internal Server Error',
        contentType: 'Text/plain',
      })
    }

    const hasPolicies = policies.length
      ? policies.some((policy) =>
          user.role.policies.some((userPolicy) => userPolicy.name === policy),
        )
      : true;
    if (!hasPolicies) return formatResponse({
      statusCode: 401,
      response: 'Unauthorized',
      contentType: 'Text/plain',
    });
  };
  return {
    before: policiesMiddlewareBefore,
  };
}
