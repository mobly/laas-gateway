import { DefaultError } from './default.error';

export class InternalServerError extends DefaultError {
  error = 'InternalServerError';
  constructor() {
    super('Internal Server Error');
    this.statusCode = 500;
  }
}
