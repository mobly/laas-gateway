export class DefaultError extends Error {
  error = 'ERROR';
  statusCode: number;
  constructor(message: string) {
    super(message);
    this.message = message;
    this.statusCode = 500;
  }

  toJson() {
    return {
      error: this.error,
      message: this.message,
    };
  }
}
