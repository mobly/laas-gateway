import { DefaultError } from './default.error';

export class TrackingNotFoundError extends DefaultError {
  error = 'TrackingNotFoundError';
  constructor() {
    super('Tracking não encontrado');
    this.message = 'Tracking não encontrado';
    this.statusCode = 404;
  }
}
