import type {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Handler,
} from 'aws-lambda';
import type { FromSchema } from 'json-schema-to-ts';

type ValidatedAPIGatewayProxyEvent<S> = Omit<APIGatewayProxyEvent, 'body'> & {
  body: FromSchema<S>;
};
export type ValidatedEventAPIGatewayProxyEvent<S> = Handler<
  ValidatedAPIGatewayProxyEvent<S>,
  APIGatewayProxyResult
>;

interface FormatJsonResponseParams<T> {
  response?: T;
  statusCode: number;
}

interface FormatJsonErrorResponseParams {
  message: string;
  statusCode: number;
}

interface FormatResponseParams {
  response: any;
  contentType: string;
  statusCode: number;
}

export const formatJSONErrorResponse = (
  { message, statusCode }: FormatJsonErrorResponseParams = {
    message: 'Internal server error',
    statusCode: 500,
  },
) => {
  return {
    statusCode,
    message,
    headers: {
      'Content-Type': 'application/json',
    },
  };
};

export function formatJSONResponse<T>(
  { statusCode, response }: FormatJsonResponseParams<T> = {
    statusCode: 204,
  },
) {
  return formatResponse({
    statusCode,
    response: JSON.stringify(response),
    contentType: 'application/json',
  });
}

export function formatResponse(
  { statusCode, response, contentType }: FormatResponseParams = {
    statusCode: 204,
    response: undefined,
    contentType: 'application/json',
  },
) {
  return {
    statusCode,
    body: response,
    headers: {
      'Content-Type': contentType,
    },
  };
}
