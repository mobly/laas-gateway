export class UserJWTDTO {
  id: string;
  name: string;
  username: string;
  enabled: boolean;
  role: RoleJWTDTO;
  phone?: string;
  email?: string;
}

export class RoleJWTDTO {
  id: string;
  name: string;
  policies: PolicyJWTDTO[];
}

export class PolicyJWTDTO {
  id: string;
  name: string;
}
