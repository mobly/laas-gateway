/* eslint-disable */
import { util, configure, Writer, Reader } from 'protobufjs/minimal';
import * as Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';

export const protobufPackage = 'order';

export enum FreightType {
  CIF = 0,
  FOB = 1,
  UNRECOGNIZED = -1,
}

export function freightTypeFromJSON(object: any): FreightType {
  switch (object) {
    case 0:
    case 'CIF':
      return FreightType.CIF;
    case 1:
    case 'FOB':
      return FreightType.FOB;
    case -1:
    case 'UNRECOGNIZED':
    default:
      return FreightType.UNRECOGNIZED;
  }
}

export function freightTypeToJSON(object: FreightType): string {
  switch (object) {
    case FreightType.CIF:
      return 'CIF';
    case FreightType.FOB:
      return 'FOB';
    default:
      return 'UNKNOWN';
  }
}

export interface GetAllParams {
  page: number;
  partnerCnpj?: string | undefined;
  from: string;
  to: string;
  limit?: number | undefined;
  ordersNumbers: string[];
  invoiceNumbers: string[];
}

export interface GetByIdParams {
  id: number;
}

export interface GetTrackingsByIdParams {
  id: string;
  partnerCnpj: string;
}

export interface GetTrackingsByIdResponse {
  occurrences: Ocurrence[];
}

export interface GetAllResponse {
  content: GetAllOrder[];
  size: number;
  totalElements: number;
  limit: number;
  page: number;
  offset: number;
  totalPages: number;
}

export interface GetAllOrder {
  id: number;
  invoice: number;
  service: string;
  status: string;
  createdAt: number;
  deadline: number;
  volumes: number;
  weight: number;
  origin: string;
  destiny: string;
  originDc: string;
  currentDc: string;
  destinyDc: string;
  freight: FreightType;
  shipperCNPJ: string;
  shipperName: string;
  lastChangeDate: number;
}

export interface Ocurrence {
  code: string;
  description: string;
  date: string;
  picturePath: string;
  signaturePath: string;
  driver: string;
  plate: string;
  city: string;
  uf: string;
}

function createBaseGetAllParams(): GetAllParams {
  return {
    page: 0,
    partnerCnpj: undefined,
    from: '',
    to: '',
    limit: undefined,
    ordersNumbers: [],
    invoiceNumbers: [],
  };
}

export const GetAllParams = {
  encode(message: GetAllParams, writer: Writer = Writer.create()): Writer {
    if (message.page !== 0) {
      writer.uint32(8).int32(message.page);
    }
    if (message.partnerCnpj !== undefined) {
      writer.uint32(18).string(message.partnerCnpj);
    }
    if (message.from !== '') {
      writer.uint32(26).string(message.from);
    }
    if (message.to !== '') {
      writer.uint32(34).string(message.to);
    }
    if (message.limit !== undefined) {
      writer.uint32(40).int32(message.limit);
    }
    for (const v of message.ordersNumbers) {
      writer.uint32(50).string(v!);
    }
    for (const v of message.invoiceNumbers) {
      writer.uint32(58).string(v!);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GetAllParams {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAllParams();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.page = reader.int32();
          break;
        case 2:
          message.partnerCnpj = reader.string();
          break;
        case 3:
          message.from = reader.string();
          break;
        case 4:
          message.to = reader.string();
          break;
        case 5:
          message.limit = reader.int32();
          break;
        case 6:
          message.ordersNumbers.push(reader.string());
          break;
        case 7:
          message.invoiceNumbers.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAllParams {
    const message = createBaseGetAllParams();
    message.page = isSet(object.page) ? Number(object.page) : 0;
    message.partnerCnpj = isSet(object.partnerCnpj)
      ? String(object.partnerCnpj)
      : undefined;
    message.from = isSet(object.from) ? String(object.from) : '';
    message.to = isSet(object.to) ? String(object.to) : '';
    message.limit = isSet(object.limit) ? Number(object.limit) : undefined;
    message.ordersNumbers = Array.isArray(object?.ordersNumbers)
      ? object.ordersNumbers.map((e: any) => String(e))
      : [];
    message.invoiceNumbers = Array.isArray(object?.invoiceNumbers)
      ? object.invoiceNumbers.map((e: any) => String(e))
      : [];
    return message;
  },

  toJSON(message: GetAllParams): unknown {
    const obj: any = {};
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.partnerCnpj !== undefined &&
      (obj.partnerCnpj = message.partnerCnpj);
    message.from !== undefined && (obj.from = message.from);
    message.to !== undefined && (obj.to = message.to);
    message.limit !== undefined && (obj.limit = Math.round(message.limit));
    if (message.ordersNumbers) {
      obj.ordersNumbers = message.ordersNumbers.map((e) => e);
    } else {
      obj.ordersNumbers = [];
    }
    if (message.invoiceNumbers) {
      obj.invoiceNumbers = message.invoiceNumbers.map((e) => e);
    } else {
      obj.invoiceNumbers = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAllParams>, I>>(
    object: I,
  ): GetAllParams {
    const message = createBaseGetAllParams();
    message.page = object.page ?? 0;
    message.partnerCnpj = object.partnerCnpj ?? undefined;
    message.from = object.from ?? '';
    message.to = object.to ?? '';
    message.limit = object.limit ?? undefined;
    message.ordersNumbers = object.ordersNumbers?.map((e) => e) || [];
    message.invoiceNumbers = object.invoiceNumbers?.map((e) => e) || [];
    return message;
  },
};

function createBaseGetByIdParams(): GetByIdParams {
  return { id: 0 };
}

export const GetByIdParams = {
  encode(message: GetByIdParams, writer: Writer = Writer.create()): Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GetByIdParams {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetByIdParams();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetByIdParams {
    const message = createBaseGetByIdParams();
    message.id = isSet(object.id) ? Number(object.id) : 0;
    return message;
  },

  toJSON(message: GetByIdParams): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetByIdParams>, I>>(
    object: I,
  ): GetByIdParams {
    const message = createBaseGetByIdParams();
    message.id = object.id ?? 0;
    return message;
  },
};

function createBaseGetTrackingsByIdParams(): GetTrackingsByIdParams {
  return { id: '', partnerCnpj: '' };
}

export const GetTrackingsByIdParams = {
  encode(
    message: GetTrackingsByIdParams,
    writer: Writer = Writer.create(),
  ): Writer {
    if (message.id !== '') {
      writer.uint32(10).string(message.id);
    }
    if (message.partnerCnpj !== '') {
      writer.uint32(18).string(message.partnerCnpj);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GetTrackingsByIdParams {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetTrackingsByIdParams();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.partnerCnpj = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetTrackingsByIdParams {
    const message = createBaseGetTrackingsByIdParams();
    message.id = isSet(object.id) ? String(object.id) : '';
    message.partnerCnpj = isSet(object.partnerCnpj)
      ? String(object.partnerCnpj)
      : '';
    return message;
  },

  toJSON(message: GetTrackingsByIdParams): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.partnerCnpj !== undefined &&
      (obj.partnerCnpj = message.partnerCnpj);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetTrackingsByIdParams>, I>>(
    object: I,
  ): GetTrackingsByIdParams {
    const message = createBaseGetTrackingsByIdParams();
    message.id = object.id ?? '';
    message.partnerCnpj = object.partnerCnpj ?? '';
    return message;
  },
};

function createBaseGetTrackingsByIdResponse(): GetTrackingsByIdResponse {
  return { occurrences: [] };
}

export const GetTrackingsByIdResponse = {
  encode(
    message: GetTrackingsByIdResponse,
    writer: Writer = Writer.create(),
  ): Writer {
    for (const v of message.occurrences) {
      Ocurrence.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number,
  ): GetTrackingsByIdResponse {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetTrackingsByIdResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.occurrences.push(Ocurrence.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetTrackingsByIdResponse {
    const message = createBaseGetTrackingsByIdResponse();
    message.occurrences = Array.isArray(object?.occurrences)
      ? object.occurrences.map((e: any) => Ocurrence.fromJSON(e))
      : [];
    return message;
  },

  toJSON(message: GetTrackingsByIdResponse): unknown {
    const obj: any = {};
    if (message.occurrences) {
      obj.occurrences = message.occurrences.map((e) =>
        e ? Ocurrence.toJSON(e) : undefined,
      );
    } else {
      obj.occurrences = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetTrackingsByIdResponse>, I>>(
    object: I,
  ): GetTrackingsByIdResponse {
    const message = createBaseGetTrackingsByIdResponse();
    message.occurrences =
      object.occurrences?.map((e) => Ocurrence.fromPartial(e)) || [];
    return message;
  },
};

function createBaseGetAllResponse(): GetAllResponse {
  return {
    content: [],
    size: 0,
    totalElements: 0,
    limit: 0,
    page: 0,
    offset: 0,
    totalPages: 0,
  };
}

export const GetAllResponse = {
  encode(message: GetAllResponse, writer: Writer = Writer.create()): Writer {
    for (const v of message.content) {
      GetAllOrder.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.totalElements !== 0) {
      writer.uint32(24).int32(message.totalElements);
    }
    if (message.limit !== 0) {
      writer.uint32(40).int32(message.limit);
    }
    if (message.page !== 0) {
      writer.uint32(48).int32(message.page);
    }
    if (message.offset !== 0) {
      writer.uint32(56).int32(message.offset);
    }
    if (message.totalPages !== 0) {
      writer.uint32(64).int32(message.totalPages);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GetAllResponse {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAllResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.content.push(GetAllOrder.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.totalElements = reader.int32();
          break;
        case 5:
          message.limit = reader.int32();
          break;
        case 6:
          message.page = reader.int32();
          break;
        case 7:
          message.offset = reader.int32();
          break;
        case 8:
          message.totalPages = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAllResponse {
    const message = createBaseGetAllResponse();
    message.content = Array.isArray(object?.content)
      ? object.content.map((e: any) => GetAllOrder.fromJSON(e))
      : [];
    message.size = isSet(object.size) ? Number(object.size) : 0;
    message.totalElements = isSet(object.totalElements)
      ? Number(object.totalElements)
      : 0;
    message.limit = isSet(object.limit) ? Number(object.limit) : 0;
    message.page = isSet(object.page) ? Number(object.page) : 0;
    message.offset = isSet(object.offset) ? Number(object.offset) : 0;
    message.totalPages = isSet(object.totalPages)
      ? Number(object.totalPages)
      : 0;
    return message;
  },

  toJSON(message: GetAllResponse): unknown {
    const obj: any = {};
    if (message.content) {
      obj.content = message.content.map((e) =>
        e ? GetAllOrder.toJSON(e) : undefined,
      );
    } else {
      obj.content = [];
    }
    message.size !== undefined && (obj.size = Math.round(message.size));
    message.totalElements !== undefined &&
      (obj.totalElements = Math.round(message.totalElements));
    message.limit !== undefined && (obj.limit = Math.round(message.limit));
    message.page !== undefined && (obj.page = Math.round(message.page));
    message.offset !== undefined && (obj.offset = Math.round(message.offset));
    message.totalPages !== undefined &&
      (obj.totalPages = Math.round(message.totalPages));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAllResponse>, I>>(
    object: I,
  ): GetAllResponse {
    const message = createBaseGetAllResponse();
    message.content =
      object.content?.map((e) => GetAllOrder.fromPartial(e)) || [];
    message.size = object.size ?? 0;
    message.totalElements = object.totalElements ?? 0;
    message.limit = object.limit ?? 0;
    message.page = object.page ?? 0;
    message.offset = object.offset ?? 0;
    message.totalPages = object.totalPages ?? 0;
    return message;
  },
};

function createBaseGetAllOrder(): GetAllOrder {
  return {
    id: 0,
    invoice: 0,
    service: '',
    status: '',
    createdAt: 0,
    deadline: 0,
    volumes: 0,
    weight: 0,
    origin: '',
    destiny: '',
    originDc: '',
    currentDc: '',
    destinyDc: '',
    freight: 0,
    shipperCNPJ: '',
    shipperName: '',
    lastChangeDate: 0,
  };
}

export const GetAllOrder = {
  encode(message: GetAllOrder, writer: Writer = Writer.create()): Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.invoice !== 0) {
      writer.uint32(16).int32(message.invoice);
    }
    if (message.service !== '') {
      writer.uint32(26).string(message.service);
    }
    if (message.status !== '') {
      writer.uint32(34).string(message.status);
    }
    if (message.createdAt !== 0) {
      writer.uint32(40).int64(message.createdAt);
    }
    if (message.deadline !== 0) {
      writer.uint32(48).int64(message.deadline);
    }
    if (message.volumes !== 0) {
      writer.uint32(56).int32(message.volumes);
    }
    if (message.weight !== 0) {
      writer.uint32(69).float(message.weight);
    }
    if (message.origin !== '') {
      writer.uint32(74).string(message.origin);
    }
    if (message.destiny !== '') {
      writer.uint32(82).string(message.destiny);
    }
    if (message.originDc !== '') {
      writer.uint32(90).string(message.originDc);
    }
    if (message.currentDc !== '') {
      writer.uint32(98).string(message.currentDc);
    }
    if (message.destinyDc !== '') {
      writer.uint32(106).string(message.destinyDc);
    }
    if (message.freight !== 0) {
      writer.uint32(112).int32(message.freight);
    }
    if (message.shipperCNPJ !== '') {
      writer.uint32(122).string(message.shipperCNPJ);
    }
    if (message.shipperName !== '') {
      writer.uint32(130).string(message.shipperName);
    }
    if (message.lastChangeDate !== 0) {
      writer.uint32(136).int64(message.lastChangeDate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GetAllOrder {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetAllOrder();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.invoice = reader.int32();
          break;
        case 3:
          message.service = reader.string();
          break;
        case 4:
          message.status = reader.string();
          break;
        case 5:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 6:
          message.deadline = longToNumber(reader.int64() as Long);
          break;
        case 7:
          message.volumes = reader.int32();
          break;
        case 8:
          message.weight = reader.float();
          break;
        case 9:
          message.origin = reader.string();
          break;
        case 10:
          message.destiny = reader.string();
          break;
        case 11:
          message.originDc = reader.string();
          break;
        case 12:
          message.currentDc = reader.string();
          break;
        case 13:
          message.destinyDc = reader.string();
          break;
        case 14:
          message.freight = reader.int32() as any;
          break;
        case 15:
          message.shipperCNPJ = reader.string();
          break;
        case 16:
          message.shipperName = reader.string();
          break;
        case 17:
          message.lastChangeDate = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAllOrder {
    const message = createBaseGetAllOrder();
    message.id = isSet(object.id) ? Number(object.id) : 0;
    message.invoice = isSet(object.invoice) ? Number(object.invoice) : 0;
    message.service = isSet(object.service) ? String(object.service) : '';
    message.status = isSet(object.status) ? String(object.status) : '';
    message.createdAt = isSet(object.createdAt) ? Number(object.createdAt) : 0;
    message.deadline = isSet(object.deadline) ? Number(object.deadline) : 0;
    message.volumes = isSet(object.volumes) ? Number(object.volumes) : 0;
    message.weight = isSet(object.weight) ? Number(object.weight) : 0;
    message.origin = isSet(object.origin) ? String(object.origin) : '';
    message.destiny = isSet(object.destiny) ? String(object.destiny) : '';
    message.originDc = isSet(object.originDc) ? String(object.originDc) : '';
    message.currentDc = isSet(object.currentDc) ? String(object.currentDc) : '';
    message.destinyDc = isSet(object.destinyDc) ? String(object.destinyDc) : '';
    message.freight = isSet(object.freight)
      ? freightTypeFromJSON(object.freight)
      : 0;
    message.shipperCNPJ = isSet(object.shipperCNPJ)
      ? String(object.shipperCNPJ)
      : '';
    message.shipperName = isSet(object.shipperName)
      ? String(object.shipperName)
      : '';
    message.lastChangeDate = isSet(object.lastChangeDate)
      ? Number(object.lastChangeDate)
      : 0;
    return message;
  },

  toJSON(message: GetAllOrder): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = Math.round(message.id));
    message.invoice !== undefined &&
      (obj.invoice = Math.round(message.invoice));
    message.service !== undefined && (obj.service = message.service);
    message.status !== undefined && (obj.status = message.status);
    message.createdAt !== undefined &&
      (obj.createdAt = Math.round(message.createdAt));
    message.deadline !== undefined &&
      (obj.deadline = Math.round(message.deadline));
    message.volumes !== undefined &&
      (obj.volumes = Math.round(message.volumes));
    message.weight !== undefined && (obj.weight = message.weight);
    message.origin !== undefined && (obj.origin = message.origin);
    message.destiny !== undefined && (obj.destiny = message.destiny);
    message.originDc !== undefined && (obj.originDc = message.originDc);
    message.currentDc !== undefined && (obj.currentDc = message.currentDc);
    message.destinyDc !== undefined && (obj.destinyDc = message.destinyDc);
    message.freight !== undefined &&
      (obj.freight = freightTypeToJSON(message.freight));
    message.shipperCNPJ !== undefined &&
      (obj.shipperCNPJ = message.shipperCNPJ);
    message.shipperName !== undefined &&
      (obj.shipperName = message.shipperName);
    message.lastChangeDate !== undefined &&
      (obj.lastChangeDate = Math.round(message.lastChangeDate));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GetAllOrder>, I>>(
    object: I,
  ): GetAllOrder {
    const message = createBaseGetAllOrder();
    message.id = object.id ?? 0;
    message.invoice = object.invoice ?? 0;
    message.service = object.service ?? '';
    message.status = object.status ?? '';
    message.createdAt = object.createdAt ?? 0;
    message.deadline = object.deadline ?? 0;
    message.volumes = object.volumes ?? 0;
    message.weight = object.weight ?? 0;
    message.origin = object.origin ?? '';
    message.destiny = object.destiny ?? '';
    message.originDc = object.originDc ?? '';
    message.currentDc = object.currentDc ?? '';
    message.destinyDc = object.destinyDc ?? '';
    message.freight = object.freight ?? 0;
    message.shipperCNPJ = object.shipperCNPJ ?? '';
    message.shipperName = object.shipperName ?? '';
    message.lastChangeDate = object.lastChangeDate ?? 0;
    return message;
  },
};

function createBaseOcurrence(): Ocurrence {
  return {
    code: '',
    description: '',
    date: '',
    picturePath: '',
    signaturePath: '',
    driver: '',
    plate: '',
    city: '',
    uf: '',
  };
}

export const Ocurrence = {
  encode(message: Ocurrence, writer: Writer = Writer.create()): Writer {
    if (message.code !== '') {
      writer.uint32(10).string(message.code);
    }
    if (message.description !== '') {
      writer.uint32(18).string(message.description);
    }
    if (message.date !== '') {
      writer.uint32(26).string(message.date);
    }
    if (message.picturePath !== '') {
      writer.uint32(34).string(message.picturePath);
    }
    if (message.signaturePath !== '') {
      writer.uint32(42).string(message.signaturePath);
    }
    if (message.driver !== '') {
      writer.uint32(50).string(message.driver);
    }
    if (message.plate !== '') {
      writer.uint32(58).string(message.plate);
    }
    if (message.city !== '') {
      writer.uint32(66).string(message.city);
    }
    if (message.uf !== '') {
      writer.uint32(74).string(message.uf);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): Ocurrence {
    const reader = input instanceof Reader ? input : new Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseOcurrence();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.code = reader.string();
          break;
        case 2:
          message.description = reader.string();
          break;
        case 3:
          message.date = reader.string();
          break;
        case 4:
          message.picturePath = reader.string();
          break;
        case 5:
          message.signaturePath = reader.string();
          break;
        case 6:
          message.driver = reader.string();
          break;
        case 7:
          message.plate = reader.string();
          break;
        case 8:
          message.city = reader.string();
          break;
        case 9:
          message.uf = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Ocurrence {
    const message = createBaseOcurrence();
    message.code = isSet(object.code) ? String(object.code) : '';
    message.description = isSet(object.description)
      ? String(object.description)
      : '';
    message.date = isSet(object.date) ? String(object.date) : '';
    message.picturePath = isSet(object.picturePath)
      ? String(object.picturePath)
      : '';
    message.signaturePath = isSet(object.signaturePath)
      ? String(object.signaturePath)
      : '';
    message.driver = isSet(object.driver) ? String(object.driver) : '';
    message.plate = isSet(object.plate) ? String(object.plate) : '';
    message.city = isSet(object.city) ? String(object.city) : '';
    message.uf = isSet(object.uf) ? String(object.uf) : '';
    return message;
  },

  toJSON(message: Ocurrence): unknown {
    const obj: any = {};
    message.code !== undefined && (obj.code = message.code);
    message.description !== undefined &&
      (obj.description = message.description);
    message.date !== undefined && (obj.date = message.date);
    message.picturePath !== undefined &&
      (obj.picturePath = message.picturePath);
    message.signaturePath !== undefined &&
      (obj.signaturePath = message.signaturePath);
    message.driver !== undefined && (obj.driver = message.driver);
    message.plate !== undefined && (obj.plate = message.plate);
    message.city !== undefined && (obj.city = message.city);
    message.uf !== undefined && (obj.uf = message.uf);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Ocurrence>, I>>(
    object: I,
  ): Ocurrence {
    const message = createBaseOcurrence();
    message.code = object.code ?? '';
    message.description = object.description ?? '';
    message.date = object.date ?? '';
    message.picturePath = object.picturePath ?? '';
    message.signaturePath = object.signaturePath ?? '';
    message.driver = object.driver ?? '';
    message.plate = object.plate ?? '';
    message.city = object.city ?? '';
    message.uf = object.uf ?? '';
    return message;
  },
};

export const OrderServiceService = {
  getAll: {
    path: '/order.OrderService/GetAll',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAllParams) =>
      Buffer.from(GetAllParams.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAllParams.decode(value),
    responseSerialize: (value: GetAllResponse) =>
      Buffer.from(GetAllResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAllResponse.decode(value),
  },
  getTrackingsById: {
    path: '/order.OrderService/GetTrackingsById',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetTrackingsByIdParams) =>
      Buffer.from(GetTrackingsByIdParams.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetTrackingsByIdParams.decode(value),
    responseSerialize: (value: GetTrackingsByIdResponse) =>
      Buffer.from(GetTrackingsByIdResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      GetTrackingsByIdResponse.decode(value),
  },
  getById: {
    path: '/order.OrderService/GetById',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetByIdParams) =>
      Buffer.from(GetByIdParams.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetByIdParams.decode(value),
    responseSerialize: (value: GetAllOrder) =>
      Buffer.from(GetAllOrder.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAllOrder.decode(value),
  },
} as const;

export interface OrderServiceServer extends UntypedServiceImplementation {
  getAll: handleUnaryCall<GetAllParams, GetAllResponse>;
  getTrackingsById: handleUnaryCall<
    GetTrackingsByIdParams,
    GetTrackingsByIdResponse
  >;
  getById: handleUnaryCall<GetByIdParams, GetAllOrder>;
}

export interface OrderServiceClient extends Client {
  getAll(
    request: GetAllParams,
    callback: (error: ServiceError | null, response: GetAllResponse) => void,
  ): ClientUnaryCall;
  getAll(
    request: GetAllParams,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetAllResponse) => void,
  ): ClientUnaryCall;
  getAll(
    request: GetAllParams,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetAllResponse) => void,
  ): ClientUnaryCall;
  getTrackingsById(
    request: GetTrackingsByIdParams,
    callback: (
      error: ServiceError | null,
      response: GetTrackingsByIdResponse,
    ) => void,
  ): ClientUnaryCall;
  getTrackingsById(
    request: GetTrackingsByIdParams,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: GetTrackingsByIdResponse,
    ) => void,
  ): ClientUnaryCall;
  getTrackingsById(
    request: GetTrackingsByIdParams,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: GetTrackingsByIdResponse,
    ) => void,
  ): ClientUnaryCall;
  getById(
    request: GetByIdParams,
    callback: (error: ServiceError | null, response: GetAllOrder) => void,
  ): ClientUnaryCall;
  getById(
    request: GetByIdParams,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetAllOrder) => void,
  ): ClientUnaryCall;
  getById(
    request: GetByIdParams,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetAllOrder) => void,
  ): ClientUnaryCall;
}

export const OrderServiceClient = makeGenericClientConstructor(
  OrderServiceService,
  'order.OrderService',
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>,
  ): OrderServiceClient;
  service: typeof OrderServiceService;
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error('Value is larger than Number.MAX_SAFE_INTEGER');
  }
  return long.toNumber();
}

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (util.Long !== Long) {
  util.Long = Long as any;
  configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
