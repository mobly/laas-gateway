// package: order
// file: order.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as order_pb from "./order_pb";

interface IOrderServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    getAll: IOrderServiceService_IGetAll;
    getTrackingsById: IOrderServiceService_IGetTrackingsById;
    getById: IOrderServiceService_IGetById;
}

interface IOrderServiceService_IGetAll extends grpc.MethodDefinition<order_pb.GetAllParams, order_pb.GetAllResponse> {
    path: "/order.OrderService/GetAll";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<order_pb.GetAllParams>;
    requestDeserialize: grpc.deserialize<order_pb.GetAllParams>;
    responseSerialize: grpc.serialize<order_pb.GetAllResponse>;
    responseDeserialize: grpc.deserialize<order_pb.GetAllResponse>;
}
interface IOrderServiceService_IGetTrackingsById extends grpc.MethodDefinition<order_pb.GetTrackingsByIdParams, order_pb.GetTrackingsByIdResponse> {
    path: "/order.OrderService/GetTrackingsById";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<order_pb.GetTrackingsByIdParams>;
    requestDeserialize: grpc.deserialize<order_pb.GetTrackingsByIdParams>;
    responseSerialize: grpc.serialize<order_pb.GetTrackingsByIdResponse>;
    responseDeserialize: grpc.deserialize<order_pb.GetTrackingsByIdResponse>;
}
interface IOrderServiceService_IGetById extends grpc.MethodDefinition<order_pb.GetByIdParams, order_pb.GetAllOrder> {
    path: "/order.OrderService/GetById";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<order_pb.GetByIdParams>;
    requestDeserialize: grpc.deserialize<order_pb.GetByIdParams>;
    responseSerialize: grpc.serialize<order_pb.GetAllOrder>;
    responseDeserialize: grpc.deserialize<order_pb.GetAllOrder>;
}

export const OrderServiceService: IOrderServiceService;

export interface IOrderServiceServer extends grpc.UntypedServiceImplementation {
    getAll: grpc.handleUnaryCall<order_pb.GetAllParams, order_pb.GetAllResponse>;
    getTrackingsById: grpc.handleUnaryCall<order_pb.GetTrackingsByIdParams, order_pb.GetTrackingsByIdResponse>;
    getById: grpc.handleUnaryCall<order_pb.GetByIdParams, order_pb.GetAllOrder>;
}

export interface IOrderServiceClient {
    getAll(request: order_pb.GetAllParams, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllResponse) => void): grpc.ClientUnaryCall;
    getAll(request: order_pb.GetAllParams, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllResponse) => void): grpc.ClientUnaryCall;
    getAll(request: order_pb.GetAllParams, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllResponse) => void): grpc.ClientUnaryCall;
    getTrackingsById(request: order_pb.GetTrackingsByIdParams, callback: (error: grpc.ServiceError | null, response: order_pb.GetTrackingsByIdResponse) => void): grpc.ClientUnaryCall;
    getTrackingsById(request: order_pb.GetTrackingsByIdParams, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: order_pb.GetTrackingsByIdResponse) => void): grpc.ClientUnaryCall;
    getTrackingsById(request: order_pb.GetTrackingsByIdParams, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: order_pb.GetTrackingsByIdResponse) => void): grpc.ClientUnaryCall;
    getById(request: order_pb.GetByIdParams, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllOrder) => void): grpc.ClientUnaryCall;
    getById(request: order_pb.GetByIdParams, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllOrder) => void): grpc.ClientUnaryCall;
    getById(request: order_pb.GetByIdParams, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllOrder) => void): grpc.ClientUnaryCall;
}

export class OrderServiceClient extends grpc.Client implements IOrderServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public getAll(request: order_pb.GetAllParams, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllResponse) => void): grpc.ClientUnaryCall;
    public getAll(request: order_pb.GetAllParams, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllResponse) => void): grpc.ClientUnaryCall;
    public getAll(request: order_pb.GetAllParams, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllResponse) => void): grpc.ClientUnaryCall;
    public getTrackingsById(request: order_pb.GetTrackingsByIdParams, callback: (error: grpc.ServiceError | null, response: order_pb.GetTrackingsByIdResponse) => void): grpc.ClientUnaryCall;
    public getTrackingsById(request: order_pb.GetTrackingsByIdParams, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: order_pb.GetTrackingsByIdResponse) => void): grpc.ClientUnaryCall;
    public getTrackingsById(request: order_pb.GetTrackingsByIdParams, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: order_pb.GetTrackingsByIdResponse) => void): grpc.ClientUnaryCall;
    public getById(request: order_pb.GetByIdParams, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllOrder) => void): grpc.ClientUnaryCall;
    public getById(request: order_pb.GetByIdParams, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllOrder) => void): grpc.ClientUnaryCall;
    public getById(request: order_pb.GetByIdParams, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: order_pb.GetAllOrder) => void): grpc.ClientUnaryCall;
}
