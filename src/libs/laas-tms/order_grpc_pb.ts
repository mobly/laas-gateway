// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var order_pb = require('./order_pb.ts');

function serialize_order_GetAllOrder(arg) {
  if (!(arg instanceof order_pb.GetAllOrder)) {
    throw new Error('Expected argument of type order.GetAllOrder');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_order_GetAllOrder(buffer_arg) {
  return order_pb.GetAllOrder.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_order_GetAllParams(arg) {
  if (!(arg instanceof order_pb.GetAllParams)) {
    throw new Error('Expected argument of type order.GetAllParams');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_order_GetAllParams(buffer_arg) {
  return order_pb.GetAllParams.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_order_GetAllResponse(arg) {
  if (!(arg instanceof order_pb.GetAllResponse)) {
    throw new Error('Expected argument of type order.GetAllResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_order_GetAllResponse(buffer_arg) {
  return order_pb.GetAllResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_order_GetByIdParams(arg) {
  if (!(arg instanceof order_pb.GetByIdParams)) {
    throw new Error('Expected argument of type order.GetByIdParams');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_order_GetByIdParams(buffer_arg) {
  return order_pb.GetByIdParams.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_order_GetTrackingsByIdParams(arg) {
  if (!(arg instanceof order_pb.GetTrackingsByIdParams)) {
    throw new Error('Expected argument of type order.GetTrackingsByIdParams');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_order_GetTrackingsByIdParams(buffer_arg) {
  return order_pb.GetTrackingsByIdParams.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_order_GetTrackingsByIdResponse(arg) {
  if (!(arg instanceof order_pb.GetTrackingsByIdResponse)) {
    throw new Error('Expected argument of type order.GetTrackingsByIdResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_order_GetTrackingsByIdResponse(buffer_arg) {
  return order_pb.GetTrackingsByIdResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var OrderServiceService = exports.OrderServiceService = {
  getAll: {
    path: '/order.OrderService/GetAll',
    requestStream: false,
    responseStream: false,
    requestType: order_pb.GetAllParams,
    responseType: order_pb.GetAllResponse,
    requestSerialize: serialize_order_GetAllParams,
    requestDeserialize: deserialize_order_GetAllParams,
    responseSerialize: serialize_order_GetAllResponse,
    responseDeserialize: deserialize_order_GetAllResponse,
  },
  getTrackingsById: {
    path: '/order.OrderService/GetTrackingsById',
    requestStream: false,
    responseStream: false,
    requestType: order_pb.GetTrackingsByIdParams,
    responseType: order_pb.GetTrackingsByIdResponse,
    requestSerialize: serialize_order_GetTrackingsByIdParams,
    requestDeserialize: deserialize_order_GetTrackingsByIdParams,
    responseSerialize: serialize_order_GetTrackingsByIdResponse,
    responseDeserialize: deserialize_order_GetTrackingsByIdResponse,
  },
  getById: {
    path: '/order.OrderService/GetById',
    requestStream: false,
    responseStream: false,
    requestType: order_pb.GetByIdParams,
    responseType: order_pb.GetAllOrder,
    requestSerialize: serialize_order_GetByIdParams,
    requestDeserialize: deserialize_order_GetByIdParams,
    responseSerialize: serialize_order_GetAllOrder,
    responseDeserialize: deserialize_order_GetAllOrder,
  },
};

exports.OrderServiceClient = grpc.makeGenericClientConstructor(OrderServiceService);
