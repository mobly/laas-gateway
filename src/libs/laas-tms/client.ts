import { ChannelCredentials, Metadata } from '@grpc/grpc-js';
import {
  GetAllParams,
  GetAllResponse,
  GetByIdParams,
  GetAllOrder,
  GetTrackingsByIdParams,
  GetTrackingsByIdResponse,
  OrderServiceClient,
} from './order';


const url = process.env.LAAS_TMS_URL as string;

const client = new OrderServiceClient(
  url,
  ChannelCredentials.createInsecure(),
);

export const getAll = function (
  params: GetAllParams,
  metadata?: Metadata,
): Promise<GetAllResponse> {
  return new Promise((resolve, reject) => {
    const _metadata = metadata ?? new Metadata();
    client.getAll(params, _metadata, (err, response) => {
      if (err) reject(err);
      else resolve(response);
    });
  });
};

export const getByID = function (
  params: GetByIdParams,
  metadata?: Metadata,
): Promise<GetAllOrder> {
  return new Promise((resolve, reject) => {
    const _metadata = metadata ?? new Metadata();
    client.getById(params, _metadata, (err, response) => {
      if (err) reject(err);
      else resolve(response);
    });
  });
};

export const getTrackings = function (
  params: GetTrackingsByIdParams,
  metadata?: Metadata,
): Promise<GetTrackingsByIdResponse> {
  return new Promise((resolve, reject) => {
    const _metadata = metadata ?? new Metadata();
    client.getTrackingsById(params, _metadata, (err, response) => {
      if (err) reject(err);
      else resolve(response);
    });
  });
};
