// package: order
// file: order.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class GetAllParams extends jspb.Message { 
    getPage(): number;
    setPage(value: number): GetAllParams;

    hasPartnercnpj(): boolean;
    clearPartnercnpj(): void;
    getPartnercnpj(): string | undefined;
    setPartnercnpj(value: string): GetAllParams;
    getFrom(): string;
    setFrom(value: string): GetAllParams;
    getTo(): string;
    setTo(value: string): GetAllParams;

    hasLimit(): boolean;
    clearLimit(): void;
    getLimit(): number | undefined;
    setLimit(value: number): GetAllParams;
    clearOrdersnumbersList(): void;
    getOrdersnumbersList(): Array<string>;
    setOrdersnumbersList(value: Array<string>): GetAllParams;
    addOrdersnumbers(value: string, index?: number): string;
    clearInvoicenumbersList(): void;
    getInvoicenumbersList(): Array<string>;
    setInvoicenumbersList(value: Array<string>): GetAllParams;
    addInvoicenumbers(value: string, index?: number): string;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetAllParams.AsObject;
    static toObject(includeInstance: boolean, msg: GetAllParams): GetAllParams.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetAllParams, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetAllParams;
    static deserializeBinaryFromReader(message: GetAllParams, reader: jspb.BinaryReader): GetAllParams;
}

export namespace GetAllParams {
    export type AsObject = {
        page: number,
        partnercnpj?: string,
        from: string,
        to: string,
        limit?: number,
        ordersnumbersList: Array<string>,
        invoicenumbersList: Array<string>,
    }
}

export class GetByIdParams extends jspb.Message { 
    getId(): number;
    setId(value: number): GetByIdParams;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetByIdParams.AsObject;
    static toObject(includeInstance: boolean, msg: GetByIdParams): GetByIdParams.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetByIdParams, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetByIdParams;
    static deserializeBinaryFromReader(message: GetByIdParams, reader: jspb.BinaryReader): GetByIdParams;
}

export namespace GetByIdParams {
    export type AsObject = {
        id: number,
    }
}

export class GetTrackingsByIdParams extends jspb.Message { 
    getId(): string;
    setId(value: string): GetTrackingsByIdParams;
    getPartnercnpj(): string;
    setPartnercnpj(value: string): GetTrackingsByIdParams;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetTrackingsByIdParams.AsObject;
    static toObject(includeInstance: boolean, msg: GetTrackingsByIdParams): GetTrackingsByIdParams.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetTrackingsByIdParams, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetTrackingsByIdParams;
    static deserializeBinaryFromReader(message: GetTrackingsByIdParams, reader: jspb.BinaryReader): GetTrackingsByIdParams;
}

export namespace GetTrackingsByIdParams {
    export type AsObject = {
        id: string,
        partnercnpj: string,
    }
}

export class GetTrackingsByIdResponse extends jspb.Message { 
    clearOccurrencesList(): void;
    getOccurrencesList(): Array<Ocurrence>;
    setOccurrencesList(value: Array<Ocurrence>): GetTrackingsByIdResponse;
    addOccurrences(value?: Ocurrence, index?: number): Ocurrence;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetTrackingsByIdResponse.AsObject;
    static toObject(includeInstance: boolean, msg: GetTrackingsByIdResponse): GetTrackingsByIdResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetTrackingsByIdResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetTrackingsByIdResponse;
    static deserializeBinaryFromReader(message: GetTrackingsByIdResponse, reader: jspb.BinaryReader): GetTrackingsByIdResponse;
}

export namespace GetTrackingsByIdResponse {
    export type AsObject = {
        occurrencesList: Array<Ocurrence.AsObject>,
    }
}

export class GetAllResponse extends jspb.Message { 
    clearContentList(): void;
    getContentList(): Array<GetAllOrder>;
    setContentList(value: Array<GetAllOrder>): GetAllResponse;
    addContent(value?: GetAllOrder, index?: number): GetAllOrder;
    getSize(): number;
    setSize(value: number): GetAllResponse;
    getTotalelements(): number;
    setTotalelements(value: number): GetAllResponse;
    getPages(): number;
    setPages(value: number): GetAllResponse;
    getLimit(): number;
    setLimit(value: number): GetAllResponse;
    getPage(): number;
    setPage(value: number): GetAllResponse;
    getOffset(): number;
    setOffset(value: number): GetAllResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetAllResponse.AsObject;
    static toObject(includeInstance: boolean, msg: GetAllResponse): GetAllResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetAllResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetAllResponse;
    static deserializeBinaryFromReader(message: GetAllResponse, reader: jspb.BinaryReader): GetAllResponse;
}

export namespace GetAllResponse {
    export type AsObject = {
        contentList: Array<GetAllOrder.AsObject>,
        size: number,
        totalelements: number,
        pages: number,
        limit: number,
        page: number,
        offset: number,
    }
}

export class GetAllOrder extends jspb.Message { 
    getId(): number;
    setId(value: number): GetAllOrder;
    getInvoice(): number;
    setInvoice(value: number): GetAllOrder;
    getService(): string;
    setService(value: string): GetAllOrder;
    getStatus(): string;
    setStatus(value: string): GetAllOrder;
    getCreatedat(): number;
    setCreatedat(value: number): GetAllOrder;
    getDeadline(): number;
    setDeadline(value: number): GetAllOrder;
    getVolumes(): number;
    setVolumes(value: number): GetAllOrder;
    getWeight(): number;
    setWeight(value: number): GetAllOrder;
    getOrigin(): string;
    setOrigin(value: string): GetAllOrder;
    getDestiny(): string;
    setDestiny(value: string): GetAllOrder;
    getOrigindc(): string;
    setOrigindc(value: string): GetAllOrder;
    getCurrentdc(): string;
    setCurrentdc(value: string): GetAllOrder;
    getDestinydc(): string;
    setDestinydc(value: string): GetAllOrder;
    getFreight(): FreightType;
    setFreight(value: FreightType): GetAllOrder;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetAllOrder.AsObject;
    static toObject(includeInstance: boolean, msg: GetAllOrder): GetAllOrder.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetAllOrder, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetAllOrder;
    static deserializeBinaryFromReader(message: GetAllOrder, reader: jspb.BinaryReader): GetAllOrder;
}

export namespace GetAllOrder {
    export type AsObject = {
        id: number,
        invoice: number,
        service: string,
        status: string,
        createdat: number,
        deadline: number,
        volumes: number,
        weight: number,
        origin: string,
        destiny: string,
        origindc: string,
        currentdc: string,
        destinydc: string,
        freight: FreightType,
    }
}

export class Ocurrence extends jspb.Message { 
    getCode(): string;
    setCode(value: string): Ocurrence;
    getDescription(): string;
    setDescription(value: string): Ocurrence;
    getDate(): string;
    setDate(value: string): Ocurrence;
    getPicturepath(): string;
    setPicturepath(value: string): Ocurrence;
    getSignaturepath(): string;
    setSignaturepath(value: string): Ocurrence;
    getDriver(): string;
    setDriver(value: string): Ocurrence;
    getPlate(): string;
    setPlate(value: string): Ocurrence;
    getCity(): string;
    setCity(value: string): Ocurrence;
    getUf(): string;
    setUf(value: string): Ocurrence;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Ocurrence.AsObject;
    static toObject(includeInstance: boolean, msg: Ocurrence): Ocurrence.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Ocurrence, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Ocurrence;
    static deserializeBinaryFromReader(message: Ocurrence, reader: jspb.BinaryReader): Ocurrence;
}

export namespace Ocurrence {
    export type AsObject = {
        code: string,
        description: string,
        date: string,
        picturepath: string,
        signaturepath: string,
        driver: string,
        plate: string,
        city: string,
        uf: string,
    }
}

export enum FreightType {
    CIF = 0,
    FOB = 1,
}
