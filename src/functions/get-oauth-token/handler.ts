import 'source-map-support/register';
import { middyfy } from '@libs/lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { formatJSONResponse } from '@libs/apiGateway';
import axios from 'axios';

const authorizationResourceServerUrl =
  process.env.AUTHORIZATION_RESOURCE_SERVER_URL;

const getOauthTokenHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  console.log('event', event);
  const body = JSON.parse(event.body);
  console.log('body', body);
  const authorization = event.headers['Authorization'];
  let headers = {};
  if (authorization) {
    headers = { ...headers, authorization };
  }
  console.log('headers', headers);

  try {
    const { data, status } = await axios.post(
      `${authorizationResourceServerUrl}/oauth/token`,
      body,
      {
        headers,
      },
    );

    return formatJSONResponse({
      statusCode: status,
      response: data,
    });
  } catch (e) {
    console.log(e);
    if (e.response) {
      return formatJSONResponse({
        statusCode: e.response.status,
        response: e.response.data,
      });
    }
    return formatJSONResponse({
      statusCode: 500,
      response: e,
    });
  }
};

export const main = middyfy(getOauthTokenHandler);
