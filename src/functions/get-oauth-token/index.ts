import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  name: 'LaaSGatewayGetOauthToken',
  vpc: {
    securityGroupIds: ['${ssm:/${opt:stage}/vpc/security-group/default}'],
    subnetIds: [
      '${ssm:/${opt:stage}/vpc/subnets/private/a}',
      '${ssm:/${opt:stage}/vpc/subnets/private/b}',
    ],
  },
  iamRoleStatementsName: 'LaaSGatewayGetOauthTokenRole',
  iamRoleStatements: [
    {
      Effect: 'Allow',
      Resource: '*',
      Action: [
        'ec2:CreateNetworkInterface',
        'ec2:DescribeNetworkInterfaces',
        'ec2:DetachNetworkInterface',
        'ec2:DeleteNetworkInterface',
        'ec2:DescribeSecurityGroups',
        'ec2:DescribeSubnets',
        'ec2:DescribeVpcs',
      ],
    },
  ],
  environment: {
    AUTHORIZATION_RESOURCE_SERVER_URL:
      '${self:custom.AUTHORIZATION_RESOURCE_SERVER_URL.${opt:stage}}',
  },
  events: [
    {
      http: {
        method: 'POST',
        path: '/oauth/token',
      },
    },
  ],
};
