import 'source-map-support/register';
import { middyfy } from '@libs/lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import {
  getTracking,
  GetTrackingParams,
} from '../../orders/service/orders.service';
import { formatJSONResponse } from '@libs/apiGateway';
import { GetTrackingsByIdResponse } from '@libs/laas-tms/order';

const getTrackingHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  try {
    console.log('event', event);
    const params: GetTrackingParams = {
      id: event.pathParameters['id'],
      partnerCnpj: event.pathParameters['partnerCnpj'],
    };
    const trackingOrder: GetTrackingsByIdResponse = await getTracking(params, {
      authorization: event.headers['Authorization'],
    });
    return formatJSONResponse<GetTrackingsByIdResponse>({
      statusCode: 200,
      response: trackingOrder,
    });
  } catch (e) {
    console.error(e);
    console.log('error', JSON.stringify(e));
    return formatJSONResponse<GetTrackingsByIdResponse>({
      statusCode: e.statusCode,
      response: e.toJson(),
    });
  }
};

export const main = middyfy(getTrackingHandler);
