import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  vpc: {
    securityGroupIds: ['${ssm:/${opt:stage}/vpc/security-group/default}'],
    subnetIds: [
      '${ssm:/${opt:stage}/vpc/subnets/private/a}',
      '${ssm:/${opt:stage}/vpc/subnets/private/b}',
    ],
  },
  iamRoleStatements: [
    {
      Effect: 'Allow',
      Resource: '*',
      Action: [
        'ec2:CreateNetworkInterface',
        'ec2:DescribeNetworkInterfaces',
        'ec2:DetachNetworkInterface',
        'ec2:DeleteNetworkInterface',
        'ec2:DescribeSecurityGroups',
        'ec2:DescribeSubnets',
        'ec2:DescribeVpcs',
        'sqs:SendMessage',
        'sqs:GetQueueUrl',
      ],
    },
  ],
  name: 'LaaSInitProcessingLambda',
  environment: {
    RECEIVE_LAAS_DATA_QUEUE_NAME:
      '${self:custom.RECEIVE_LAAS_DATA_QUEUE_NAME.${opt:stage}, "teste"}',
    SECURITY_OAUTH_TOKEN_DETAILS_URL:
      '${self:custom.SECURITY_OAUTH_TOKEN_DETAILS_URL.${opt:stage}, "teste"}',
    NODE_ENV: '${self:custom.NODE_ENV.${opt:stage}, "teste"}',
    SQS_ENDPOINT: '${self:custom.SQS_ENDPOINT.${opt:stage}, "teste"}',
  },
  events: [
    {
      http: {
        method: 'post',
        path: 'api/v1/criar-pedido',
      },
    },
  ],
};
