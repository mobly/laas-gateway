export interface Nfe {
  nfeProc: NfeProc;
}

export interface NfeProc {
  xmlns: string;
  versao: string;
  NFe: NFe;
  protNFe: ProtNFe;
}

export interface NFe {
  xmlns: string;
  infNFe: InfNFe;
  Signature: Signature;
}

export interface Signature {
  xmlns: string;
  SignedInfo: SignedInfo;
  SignatureValue: string;
  KeyInfo: KeyInfo;
}

export interface KeyInfo {
  X509Data: X509Data;
}

export interface X509Data {
  X509Certificate: string;
}

export interface SignedInfo {
  xmlns: string;
  CanonicalizationMethod: CanonicalizationMethod;
  SignatureMethod: CanonicalizationMethod;
  Reference: Reference;
}

export interface CanonicalizationMethod {
  Algorithm: string;
}

export interface Reference {
  URI: string;
  Transforms: Transforms;
  DigestMethod: CanonicalizationMethod;
  DigestValue: string;
}

export interface Transforms {
  Transform: CanonicalizationMethod[];
}

export interface InfNFe {
  Id: string;
  versao: string;
  ide: IDE;
  emit: Emit;
  dest: Dest;
  autXML: AutXML[];
  det: Det | Det[];
  total: Total;
  transp: Transp;
  pag: Pag;
  infAdic: InfAdic;
  infRespTec: InfRespTec;
}

export interface AutXML {
  CNPJ: string;
}

export interface Dest {
  CPF: string;
  xNome: string;
  enderDest: Ender;
  indIEDest: string;
}

export interface Ender {
  xLgr: string;
  nro: string;
  xBairro: string;
  cMun: string;
  xMun: string;
  UF: string;
  CEP: string;
  cPais: string;
  xPais: string;
  fone: string;
  xCpl: string;
}

export interface Det {
  barcode: string;
  nItem: string;
  prod: Prod;
  imposto: Imposto;
}

export interface Imposto {
  ICMS: Icms;
  IPI: Ipi;
  PIS: Pis;
  COFINS: Cofins;
  ICMSUFDest: ICMSUFDest;
}

export interface Cofins {
  COFINSOutr: COFINSOutr;
}

export interface COFINSOutr {
  CST: string;
  vBC: string;
  pCOFINS: string;
  vCOFINS: string;
}

export interface Icms {
  ICMS00: Icms00;
}

export interface Icms00 {
  orig: string;
  CST: string;
  modBC: string;
  vBC: string;
  pICMS: string;
  vICMS: string;
}

export interface ICMSUFDest {
  vBCUFDest: string;
  pICMSUFDest: string;
  pICMSInter: string;
  pICMSInterPart: string;
  vICMSUFDest: string;
  vICMSUFRemet: string;
}

export interface Ipi {
  cEnq: string;
  IPITrib: IPITrib;
}

export interface IPITrib {
  CST: string;
  vBC: string;
  pIPI: string;
  vIPI: string;
}

export interface Pis {
  PISOutr: PISOutr;
}

export interface PISOutr {
  CST: string;
  vBC: string;
  pPIS: string;
  vPIS: string;
}

export interface Prod {
  cProd: string;
  cEAN: string;
  xProd: string;
  NCM: string;
  CFOP: string;
  uCom: string;
  qCom: string;
  vUnCom: string;
  vProd: string;
  cEANTrib: string;
  uTrib: string;
  qTrib: string;
  vUnTrib: string;
  vFrete: string;
  indTot: string;
  xPed: string;
  nItemPed: string;
}

export interface Emit {
  CNPJ: string;
  xNome: string;
  xFant: string;
  enderEmit: Ender;
  IE: string;
  IEST: string;
  CRT: string;
}

export interface IDE {
  cUF: string;
  cNF: string;
  natOp: string;
  mod: string;
  serie: string;
  nNF: string;
  dhEmi: string;
  tpNF: string;
  idDest: string;
  cMunFG: string;
  tpImp: string;
  tpEmis: string;
  cDV: string;
  tpAmb: string;
  finNFe: string;
  indFinal: string;
  indPres: string;
  procEmi: string;
  verProc: string;
  NFref: NFref;
}

export interface NFref {
  refNFe: string;
}

export interface InfAdic {
  infAdFisco: string;
  infCpl: string;
}

export interface InfRespTec {
  CNPJ: string;
  xContato: string;
  email: string;
  fone: string;
}

export interface Pag {
  detPag: DetPag;
}

export interface DetPag {
  tPag: string;
  vPag: string;
}

export interface Total {
  ICMSTot: ICMSTot;
}

export interface ICMSTot {
  vBC: string;
  vICMS: string;
  vICMSDeson: string;
  vICMSUFRemet: string;
  vFCP: string;
  vBCST: string;
  vST: string;
  vFCPST: string;
  vFCPSTRet: string;
  vProd: string;
  vFrete: string;
  vSeg: string;
  vDesc: string;
  vII: string;
  vIPI: string;
  vIPIDevol: string;
  vPIS: string;
  vCOFINS: string;
  vOutro: string;
  vNF: string;
}

export interface Transp {
  modFrete: string;
  transporta: Transporta;
  vol: Vol;
}

export interface Transporta {
  CNPJ: string;
  xNome: string;
  IE: string;
  xEnder: string;
  xMun: string;
  UF: string;
}

export interface Vol {
  qVol: string;
  esp: string;
  nVol: string;
  pesoL: string;
  pesoB: string;
}

export interface ProtNFe {
  xmlns: string;
  versao: string;
  infProt: InfProt;
}

export interface InfProt {
  tpAmb: string;
  verAplic: string;
  chNFe: string;
  dhRecbto: Date;
  nProt: string;
  digVal: string;
  cStat: string;
  xMotivo: string;
}
