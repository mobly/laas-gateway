export interface Delivery {
  package: string;
  carrierDocument: string;
  carrierCode: string;
  freightValue: string;
  typeShipping: string;
  customer: Customer;
  partner: Partner;
}

export interface Partner {
  cnpj: string;
  companyName: string;
  tradingName: string
}

export interface Customer {
  name: string;
  email: string;
  phone: string;
  document: string;
  address: Address;
  invoice: Invoice;
  volumes: Volume[];
}

export interface Address {
  street: string;
  number: string;
  postcode: string;
  neighborhood: string;
  city: string;
  state: string;
  complement?: string;
  reference: string;
  country: string;
  type: string;
}

export interface Invoice {
  key: string;
  originState: string;
  originZipCode: string;
  serie: string;
  number: string;
  total: string;
  date: string;
  identifier: string;
  products: Product[];
}

export interface Product {
  code: string;
  name: string;
}

export interface Volume {
  barcode: string;
  weight: string;
  cubedWeight: string;
  productCode?: string;
  length?: string;
  width: string;
  height: string;
}
