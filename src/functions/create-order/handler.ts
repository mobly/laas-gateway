import 'source-map-support/register';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { toJson } from 'xml2json';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { receiveProduct } from '@receiveProduct/service/receive-product';
import { policiesMiddleware } from '@libs/middleware/securityMiddleware';
import { Nfe } from '@functions/create-order/dto/nfe.dto';

interface Body {
  invoice: string;
  barCodes?: Barcode[];
}

interface Barcode {
  productCode: string;
  barCode: string;
}

const httpGatewayHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  try {
    const body: Body = JSON.parse(event.body);
    const normalizedBody: Body = {
      ...body,
      barCodes: body.barCodes ?? [],
    };
    const invoiceXmlString = Buffer.from(normalizedBody.invoice, 'base64').toString('utf-8');
    const transformedInvoice: Nfe = JSON.parse(toJson(invoiceXmlString));
    const products = !Array.isArray(transformedInvoice.nfeProc.NFe.infNFe.det)
      ? [transformedInvoice.nfeProc.NFe.infNFe.det]
      : transformedInvoice.nfeProc.NFe.infNFe.det;
    const productsWithBarcodes = products.map((product) => {
      const productBarcode = normalizedBody.barCodes.find(
        ({ productCode }) => productCode === product.prod.cProd,
      );
      return { ...product, barcode: productBarcode?.barCode };
    });
    transformedInvoice.nfeProc.NFe.infNFe.det = !Array.isArray(
      transformedInvoice.nfeProc.NFe.infNFe.det,
    )
      ? productsWithBarcodes[0]
      : productsWithBarcodes;
    await receiveProduct(transformedInvoice);
    return formatJSONResponse({
      statusCode: 200,
    });
  } catch (e) {
    console.error(e);
    return formatJSONResponse({
      statusCode: 500,
      response: e,
    });
  }
};

// export const main = middyfy(httpGatewayHandler);

// TODO: Começar a usar esse de baixo quando tivermos o serviço de segurança
// TODO: Também definir o nome da policy dessa rota
export const main = middyfy(httpGatewayHandler)
  .use(policiesMiddleware('@LAAS-GATEWAY/CRIAR-PEDIDO'));
