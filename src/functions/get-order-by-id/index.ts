import { handlerPath } from '@libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  vpc: {
    securityGroupIds: ['${ssm:/${opt:stage}/vpc/security-group/default}'],
    subnetIds: [
      '${ssm:/${opt:stage}/vpc/subnets/private/a}',
      '${ssm:/${opt:stage}/vpc/subnets/private/b}',
    ],
  },
  iamRoleStatements: [
    {
      Effect: 'Allow',
      Resource: '*',
      Action: [
        'ec2:CreateNetworkInterface',
        'ec2:DescribeNetworkInterfaces',
        'ec2:DetachNetworkInterface',
        'ec2:DeleteNetworkInterface',
        'ec2:DescribeSecurityGroups',
        'ec2:DescribeSubnets',
        'ec2:DescribeVpcs',
        'sqs:SendMessage',
        'sqs:GetQueueUrl',
      ],
    },
  ],
  name: 'LaaSGetOrderById',
  environment: {
    LAAS_TMS_URL:
      '${self:custom.LAAS_TMS_URL.${opt:stage}, ""}',
    NODE_ENV: '${self:custom.NODE_ENV.${opt:stage}, "teste"}',
  },
  events: [
    {
      http: {
        method: 'GET',
        path: 'api/v1/pedido/{id}',
      },
    },
  ],
};
