import 'source-map-support/register';
import { formatJSONResponse } from '@libs/apiGateway';
import { middyfy } from '@libs/lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { getOrderById } from '../../orders/service/orders.service';
import { GetAllOrder } from '@libs/laas-tms/order';


const getOrderByIdHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  try {
    const { id } = event.pathParameters;
    const authorization  = event.headers['Authorization'];

    const orderById: GetAllOrder = await getOrderById(Number(id), {
      authorization,
    });

    return formatJSONResponse<GetAllOrder>({
      statusCode: 200,
      response: orderById,
    });
  } catch (e) {
    console.error(e);
    return formatJSONResponse({
      statusCode: 500,
      response: e,
    });
  }
};

// export const main = middyfy(httpGatewayHandler);

// TODO: Começar a usar esse de baixo quando tivermos o serviço de segurança
// TODO: Também definir o nome da policy dessa rota
export const main = middyfy(getOrderByIdHandler);
// .use(policiesMiddleware('@LAAS-GATEWAY/CRIAR-PEDIDO'));
