import 'source-map-support/register';
import { middyfy } from '@libs/lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { getOrders, GetOrdersParams } from '@orders/service/orders.service';
import { formatJSONResponse } from '@libs/apiGateway';
import { GetAllResponse } from '@libs/laas-tms/order';

const getAllOrdersHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  try {
    console.log('event', event);
    const params: GetOrdersParams = {
      partnerCnpj: event.queryStringParameters['partnerCnpj'],
      from: event.queryStringParameters['from'],
      to: event.queryStringParameters['to'],
      invoiceNumbers: event.multiValueQueryStringParameters['invoiceNumbers'],
      ordersNumbers: event.multiValueQueryStringParameters['ordersNumbers'],
      page: event.queryStringParameters['page']
        ? Number(event.queryStringParameters['page'])
        : undefined,
      limit: event.queryStringParameters['limit']
        ? Number(event.queryStringParameters['limit'])
        : undefined,
    };
    console.log('params', params);
    const orders: GetAllResponse = await getOrders(params, {
      authorization: event.headers['Authorization'],
    });
    console.log('orders', orders);
    return formatJSONResponse<GetAllResponse>({
      statusCode: 200,
      response: orders,
    });
  } catch (e) {
    console.log(e);
    return formatJSONResponse<GetAllResponse>({
      statusCode: 500,
    });
  }
};

export const main = middyfy(getAllOrdersHandler);
