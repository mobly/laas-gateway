import 'source-map-support/register';
import { middyfy } from '@libs/lambda';
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { getOrders, GetOrdersParams } from '@orders/service/orders.service';
import { formatJSONResponse } from '@libs/apiGateway';
import { GetAllResponse } from '@libs/laas-tms/order';


const getOrdersHandler = async (
  event: APIGatewayProxyEvent,
): Promise<APIGatewayProxyResult> => {
  try {
  const params: GetOrdersParams = {
    partnerCnpj: event.pathParameters['partnerCnpj'],
    from: event.queryStringParameters['from'],
    to: event.queryStringParameters['to'],
    invoiceNumbers: event.multiValueQueryStringParameters['invoiceNumbers'],
    ordersNumbers: event.multiValueQueryStringParameters['ordersNumbers'],
    page: event.queryStringParameters['page']
      ? Number(event.queryStringParameters['page'])
      : undefined,
    limit: event.queryStringParameters['limit']
      ? Number(event.queryStringParameters['limit'])
      : undefined,
  };
  const orders: GetAllResponse = await getOrders(params, {
    authorization: event.headers['Authorization'],
  });
  return formatJSONResponse<GetAllResponse>({
    statusCode: 200,
    response: orders,
  });
  } catch (e) {
    console.error(e);
    console.log('erro', JSON.stringify(e));
    return formatJSONResponse<GetAllResponse>({
     statusCode: 500,
    });
  }
};

export const main = middyfy(getOrdersHandler);
