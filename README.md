# @laas/gateway

> Porta de entrada para integrações de terceiros com a Mobly

## Sobre

A ideia é que o LaaS seja a porta de entrada de terceiros com a Mobly, certo? Mas mesmo a porta de entrada precisa
separar de fato a porta de entrada do processamento que fazemos, então o @laas/gateway é pra isso!

Todo endpoint que expormos para terceiros ficará aqui, e o @laas/gateway vai fazer a integração necessária para fazer o
processamento.

## Tecnologias

Esse projeto foi feito com o template `aws-nodejs-typescript` do [Serverless framework](https://www.serverless.com/)

## Instalação

Por causa de ser uma lambda, o projeto precisa estar em alguma versão menor do que a versão `14.15.0` do NodeJS. Versões
Major maiores do que essa não vão conseguir subir a aplicação

### Rodando o projeto

- Rode `npm i` para instalar as dependências
- Rode `docker-compose up -d` para subir a infraestrutura necessária
- Rode `npm run start` para rodar localmente

A partir daí, você pode mudar o código o quanto quiser, já que o `npm run start` reinicia o projeto a cada alteração
feita dentro da pasta `src`!

## Estrutura do projeto

A maior parte do código fica na pasta `src`. Dentro do `src`, tem essas divisões

- `functions` - Códigos relacionados a cada lambda específica, como regra de negócio e criação da Lambda
- `libs` - Códigos que podem ser usados em várias lambdas

```
.
├── src
│   ├── functions               # Configuração e código da lambda
│   │   ├── http-gateway
│   │   │   │── dto             # Aqui fica todos os DTO's dessa lambda
│   │   │   ├── handler.ts      # Código da lambda
│   │   │   ├── index.ts        # Configuração da Lambda
│   │   │
│   │   └── index.ts            # Importação e Exportação de todos os códigos
│   │
│   └── libs                    # Códigos reutilizáveis
│       └── dto                 # Aqui fica todos os DTO's que são reutilizáveis entre as Lambdas
│       └── middleware          # Qualquer middleware para as lambdas fica aqui
│         └── securityMiddleware.ts     # Middleware se checa se o usuário está autenticado, e se tem a permissão para chamar essa lambda
│       └── apiGateway.ts       # Helpers para API Gateway
│       └── getQueueProducer.ts # Helper para criar producer para filas
│       └── handlerResolver.ts  # Código para descobrir import das Lambdas
│       └── lambda.ts           # Middy middleware para lambdas
│
├── package.json
├── serverless.ts               # Arquivo de configuração do Serverless Framework
├── tsconfig.json               # Configuração do TS
├── tsconfig.paths.json         # Configuração do TS para alias das pastas
└── webpack.config.js           # Configuração do webpack
```
