import type { AWS } from '@serverless/typescript';
import {
  GetOrders,
  GetOrderTracking,
  GetOrdersById,
  GetAllOrders,
  GetOauthToken,
  CreateOrder,
} from '@functions/index';

const serverlessConfiguration: AWS = {
  service: 'laas-gateway',
  variablesResolutionMode: '20210326',
  frameworkVersion: '2',
  useDotenv: true,
  custom: {
    RECEIVE_LAAS_DATA_QUEUE_NAME: {
      staging: 'laas-receiveLaaSData',
      prod: '',
    },
    SQS_ENDPOINT: {
      staging: 'https://sqs.us-east-1.amazonaws.com',
      prod: '',
    },
    SECURITY_OAUTH_TOKEN_DETAILS_URL: {
      staging: 'https://laas-security.mobly-stg.com.br/oauth/token/details',
      prod: '',
    },
    LAAS_TMS_URL: {
      staging: 'grpc-tms.moblystg-svc.aws:5000',
      prod: 'laas-tms-grpc.mobly-svc.aws:5000',
    },
    NODE_ENV: {
      staging: 'PRODUCTION',
      prod: 'PRODUCTION',
    },
    AUTHORIZATION_RESOURCE_SERVER_URL: {
      staging: 'https://laas-authorization-resource.mobly-stg.com.br',
      prod: 'http://laas-authorization-resource.mobly.aws',
    },
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: true,
    },
    'serverless-offline': {
      useChildProcesses: true,
      httpPort: 4000,
      stageVariables: {
        NODE_ENV: 'development',
        RECEIVE_LAAS_DATA_QUEUE_NAME: 'receiveLaaSData',
        SECURITY_OAUTH_TOKEN_DETAILS_URL: '',
      },
    },
  },
  plugins: [
    'serverless-iam-roles-per-function',
    'serverless-webpack',
    'serverless-offline',
  ],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    region: 'us-east-1',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
    },
    lambdaHashingVersion: '20201221',
  },
  functions: {
    GetOrders,
    GetOrderTracking,
    GetOrdersById,
    GetAllOrders,
    GetOauthToken,
    CreateOrder,
  },
};

module.exports = serverlessConfiguration;
